public class Main {
   public static void main(String[] args) {
	    boolean keepDrawing = true;
	    UserInput userInput = new UserInput();
		while(keepDrawing) {
			int length = userInput.getIntFromUser("Insert the length of the square");
			int width = userInput.getIntFromUser("Insert the width of the square");
			System.out.println("Inserted length and width: " + length + ", " + width);
			DrawRectangle drawer = new DrawRectangle(width, length);
			drawer.draw();
			keepDrawing = userInput.getYesOrNoFromUser("Want to draw another rectangle?");
		}
		userInput.closeScanner();
   }
}