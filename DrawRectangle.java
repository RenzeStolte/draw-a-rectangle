public class DrawRectangle {
	private int width;
	private int length;
	private boolean drawMiddleSquare; // Is true if there is room for an inner rectangle.
	
	public DrawRectangle(int width, int length) {
		this.width = width;
		this.length = length;
	}
	
	/**
     * Draws a rectangle. If the width and length is large enough, it will draw an additional rectangle inside
     * of it. The inner rectangle will always be drawn one square away from the outer rectangle. 
     */
	public void draw() {
		drawMiddleSquare = (width >= 5 && length >= 5);
		
		for(int i = 0; i < width; i ++) {
			if(i == 0 || i == width - 1)
				drawLine();
			else if(i == 1 || i == width - 2) {
				drawMiddle();
			}
			else if(drawMiddleSquare) {
				drawOuterLeftSide();
				if(i == 2 || i == width - 3) {
					drawInnerLine();
				} else {
					drawInnerMiddle();
				}
				drawOuterRightSide();
			} else {
				drawMiddle();
			}
		}
	}
	
	/**
	 * Draws a number of #'s equal to the length of the rectangle.
	 * ###...###
	 */
	private void drawLine() {
		for(int i = 0; i < length; i++) {
			System.out.print("#");
		}
		System.out.print("\n");
	}
	
	/**
	 * Draws a number of #'s equal to the length - 4 of the rectangle.
	 * --###...###--
	 */
	private void drawInnerLine() {
		for(int i = 2; i < length - 2; i++) {
			System.out.print("#");
		}
	}
	
	
	/**
	 * Draws a #, then a number of empty characters, then another #, in total filling an entire row of the rectangle
	 * #   ...   #
	 */
	private void drawMiddle() {
		System.out.print("#");
		for(int i = 1; i < length - 1; i++) {
			System.out.print(" ");
		}
		System.out.print("#\n");
	}
	
	/**
	 * Draws a #, then a number of empty characters, then another #, in total filling an entire row except for 4 places
	 * --#   ...   #--
	 */
	private void drawInnerMiddle() {
		for(int i = 2; i < length - 2; i++) {
			if(i == 2 || i == length - 3) {
				System.out.print("#");
			} else {
				System.out.print(" ");
			}
		}
	}
	
	private void drawOuterLeftSide() {
		System.out.print("# ");
	}
	
	private void drawOuterRightSide() {
		System.out.print(" #\n");
	}
}
