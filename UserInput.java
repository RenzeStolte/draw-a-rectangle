import java.util.Scanner;
import java.util.NoSuchElementException;

/*
 * UserInput has functions related to asking and taking input from the user.
 */
public class UserInput {
	private Scanner scanner;
	
	public UserInput() {
		scanner = new Scanner(System.in);
	}
	
	/**	
	 * Keeps asking the provided query until the user has given a valid integer that is greater than zero.
	 */	
	public int getIntFromUser(String query) {
		System.out.println(query);
		int userInt;
		
		try {
			userInt = scanner.nextInt();
		} catch (NoSuchElementException e) {
			System.out.println("Please enter a valid integer.");
			scanner.nextLine();
			return getIntFromUser(query);
		}
		
		if(userInt < 0) {
			System.out.println("To make a box, size must be larger than 0. Please insert a different size.");
			return getIntFromUser(query);
		}
		
		return userInt;
	}
	
	/**
	 * Keeps asking the provided query until the user has responded with a string that starts with y or n.
	 */
	public boolean getYesOrNoFromUser(String query) {
		System.out.print(query);
		System.out.println(" Type y for yes, n for no");
		String userRespons;
		
		try {
			userRespons = scanner.next();
		} catch (NoSuchElementException e) {
			System.out.println("Please enter a valid resonse.");
			scanner.nextLine();
			return getYesOrNoFromUser(query);
		}
		
		switch(userRespons.charAt(0)) {
			case 'y':
				return true;
			case 'n':
				return false;
			default:
				System.out.println("Please respond with y or n");
				return getYesOrNoFromUser(query);
		}
	}
	
	/**
	 * Closes the scanner. Unsure if this is neccesary, but included anyways.
	 */
	public void closeScanner() {
		scanner.close();
	}
}